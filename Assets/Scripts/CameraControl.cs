﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public GameObject player1;
    public GameObject player2;
    private Vector3 offset;
    
	void Start()
    {
		
		offset = transform.position - GetMidpoint();
	}
	
	void LateUpdate()
    {
		transform.position = GetMidpoint() + offset;
	}

	Vector3 GetMidpoint()
	{
		return Vector3.Lerp(player1.transform.position, player2.transform.position, 0.5f);
	}
}
