﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GolemScript : MonoBehaviour
{
	private readonly Vector3 startPosition = new Vector3(3.6f, 2.1f, 20f);

	public float speed = 1;
	public GameObject gameManager;

	private Animator anim;
	private ManagerScript managerScript;
	//private CharacterController controller;

	private GameObject activator;

	// Use this for initialization
	void Start()
	{
		anim = GetComponent<Animator>();
		managerScript = gameManager.GetComponent<ManagerScript>();
		//controller = GetComponent<CharacterController>();
	}

	void FixedUpdate()
	{
		//Movement
		float vertRaw = Input.GetAxisRaw("P1Vertical");
		float horizRaw = Input.GetAxisRaw("P1Horizontal");

		if (Mathf.Abs(vertRaw) > 0 || Mathf.Abs(horizRaw) > 0)
		{
			GameObject mt = GameObject.Find("MoveText");
			if (mt != null) mt.SetActive(false);

			Vector3 xDir = new Vector3(-horizRaw, 0, -horizRaw);
			Vector3 yDir = new Vector3(vertRaw, 0, -vertRaw);
			transform.rotation = Quaternion.LookRotation(xDir + yDir);
			transform.Translate(new Vector3(0, 0, speed));
			//controller.SimpleMove(transform.forward * speed);
		}
		/*else
		{
			controller.SimpleMove(Vector3.zero);
		}*/

		//Activating platforms
		if (Input.GetKeyDown(KeyCode.Space) && activator != null)
			ActivateStuff(activator.gameObject.name);
	}

	void ActivateStuff(string name)
	{
		//Bridge platform
		if (name.Equals("BridgePlatform") && managerScript.HaveEnoughEnergy())
		{
			GameObject bt = GameObject.Find("BridgeText");
			if (bt != null) bt.SetActive(false);

			BridgeScript s = GameObject.Find("SecretBridge").GetComponent<BridgeScript>();
			if (s != null) s.Raise();
		}
		//Low platform
		else if (name.Equals("TeleportPlatform1"))
		{
			GameObject tt = GameObject.Find("TeleText");
			if (tt != null) tt.SetActive(false);

			transform.position = new Vector3(8.2f, 5f, 6.5f);
		}
		//High platform
		else if (name.Equals("TeleportPlatform2"))
			transform.position = new Vector3(0.9f, 2.1f, 6.6f);
	}

	void Update()
	{
		//Animation
		float vertRaw = Input.GetAxisRaw("P1Vertical");
		float horizRaw = Input.GetAxisRaw("P1Horizontal");

		if (Mathf.Abs(vertRaw) > 0 || Mathf.Abs(horizRaw) > 0)
			anim.SetBool("walking", true);
		else
			anim.SetBool("walking", false);
	}

	void OnTriggerEnter(Collider other)
	{
		//Reset position if player hits taint
		if (other.gameObject.tag.Equals("Taint"))
		{
			transform.position = startPosition;
			transform.rotation = Quaternion.identity;
			transform.Rotate(transform.up, 180);
		}
		//Collect energy
		else if (other.gameObject.tag.Equals("Energy"))
		{
			managerScript.GetEnergy();
			
			if (other.gameObject.name.Equals("EnergyPickup1"))
			{
				GameObject et = GameObject.Find("EnergyText");
				if (et != null) et.SetActive(false);
			}
			
			other.gameObject.SetActive(false);
		}
		//Set the current overlapping activator
		else if (other.gameObject.tag.Equals("Activator"))
			activator = other.gameObject;
		else if (other.gameObject.name.Equals("Fountain"))
			managerScript.WinGame();
	}

	void OnTriggerExit(Collider other)
	{
		//When no longer overlapping with activator
		if (other.gameObject.tag.Equals("Activator"))
			activator = null;
	}

	void OnCollisionEnter(Collision other)
	{
		//When pushing wall remove the text
		if (other.gameObject.name.StartsWith("Wall"))
		{
			GameObject wt = GameObject.Find("WallText1");
			if (wt != null && wt.activeSelf.Equals(true))
				wt.SetActive(false);

			wt = GameObject.Find("WallText2");
			if (wt != null && wt.activeSelf.Equals(true))
				wt.SetActive(false);
		}
	}
}