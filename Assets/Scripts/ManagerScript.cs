﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerScript : MonoBehaviour {

	private int acquiredEnergy = 0;
	private readonly int requiredEnergy = 6;

	public Text energyText;
	public Text winText;
	public Material energyMat;

	void OnGUI()
	{
		energyText.text = "Energy: " + acquiredEnergy + "/" + requiredEnergy;
	}

	public void GetEnergy()
	{
		acquiredEnergy++;

		if (HaveEnoughEnergy())
		{
			GameObject bp = GameObject.Find("BridgePlatform");
			if (bp != null)
			{
				Renderer r = bp.GetComponent<Renderer>();
				r.material = energyMat;
			}
		}
	}

	public bool HaveEnoughEnergy()
	{
		return acquiredEnergy >= requiredEnergy;
	}

	public void WinGame()
	{
		winText.gameObject.SetActive(true);
	}
}
