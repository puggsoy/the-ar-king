﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeScript : MonoBehaviour {

	private bool raising = false;
	private readonly Vector3 raisedPosition = new Vector3(2.05f, 1.09f, -10.34f);

	public void Raise()
	{
		raising = true;
	}

	void FixedUpdate()
	{
		if (raising && !transform.position.Equals(raisedPosition))
		{
			transform.position = Vector3.MoveTowards(transform.position, raisedPosition, 1 * Time.fixedDeltaTime);
		}
	}
}
